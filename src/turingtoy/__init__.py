from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)

def clean_blank(input_, blank):
    i = 0
    while(input_[i]==blank):
        input_.pop(i)
        i+=1
    i = 0
    len_input = len(input_)
    while(input_[len_input-i-1]==blank):
        input_.pop(len_input-i-1)
        i+=1
    return input_

def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    # for debugging
    print(f"Machine : {machine}\ninput : {input_}\nsteps: {steps}")
    blank = machine['blank']
    start = machine['start state']
    end = machine['final states']
    table = machine['table']
    states = machine['table'].keys
    current_state = start 
    step = 0
    input_ = list(input_)
    pos = 0
    old_pos = 0
    hist = []
    while current_state not in end:
        if steps is not None and step >= steps:
            break
        if pos < 0:
            pos = 0
            input_.insert(0,blank)
        elif pos >= len(input_):
            input_.append(blank)

        symbol = input_[start]
        if symbol not in table[current_state]:
            break

        trans = table[current_state][symbol]
        hist_dic = {
                "state": current_state,
                "symbol": symbol,
                "position": pos,
                "memory":''.join(table),
                "transition": trans 
                }
        hist.append(hist_dic)
        if trans == "L":
            pos -= 1
        elif trans == "R":
            pos += 1

        else:
            if "write" in trans:
                table[pos] = trans['write']
            if "L" in trans:
                pos -= 1
                current_state = trans['L']
            elif "R" in trans:
                pos += 1
                current_state = trans['R']
            else:
                pos += 0
                break

        step += 1

    result = "".join(table).strip(blank)
    return (result, hist, current_state in end)
